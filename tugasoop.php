<?php

class BangunDatar {
    public $nama,
           $warna;

    public function __construct($nama = "nama", $warna = "warna")
    {
       $this->nama = $nama;
       $this->warna = $warna; 
    }

    public function setNama($nama){
        if ( !is_string($nama)){
            throw new Exception("Nama harus string");
        }
        $this->nama = $nama;
    }

    public function setWarna($warna){
        if ( !is_string($warna)){
            throw new Exception("Warna harus string");
        }
        $this->warna = $warna;
    }


    public function getLabel(){
        return "$this->nama, $this->warna";
    }
}
class PersegiPanjang extends BangunDatar{
    private $panjang;
    private $lebar;

    public function __construct($nama = "nama", $warna = "warna", $panjang = 0, $lebar = 0){
        parent::__construct($nama, $warna);

        $this->panjang = $panjang;
        $this->lebar = $lebar;
    }

    public function getInfo(){
        $str = "Persegi Panjang : " . parent::getLabel() . " | Panjang:{$this->panjang}" . " |Lebar:{$this->lebar}";
        return $str; 
    }


    public function setPanjang($panjang){
        $this->panjang = $panjang;
    }

    public function setLebar($lebar){
        $this->lebar = $lebar;
    }


   
    public function getKeliling(){
        return 2 * ($this->panjang + $this->lebar);
    }

    public function getLuas(){
        return $this->panjang*$this->lebar; 
    }

}

class Lingkaran extends BangunDatar{
    private $r;
    const PHI = 3.14;

    public function __construct($nama = "nama", $warna = "warna", $r = 0){
        parent::__construct($nama, $warna);

        $this->r = $r;
    }

    public function setr($r){
        $this->r = $r;
    }

    public function getInfo() {
        $str = "Lingkaran : " . parent::getLabel() . "| r: {$this->r}" . "| Phi: " . self::PHI;
        return $str; 
    }

    public function getKeliling(){
        return 2*$this->r* self::PHI;
    }

    public function getLuas(){
        return $this->r*$this->r* self::PHI;
    }
}

class Segitiga extends BangunDatar{
    private $alas;
    private $tinggi;
    private $sisi1, $sisi2, $sisi3;

    public function __construct($nama = "nama", $warna = "warna", $alas = 0, $tinggi = 0) {
        parent::__construct($nama, $warna);

        $this->alas = $alas;
        $this->tinggi = $tinggi;
    }

    public function setSisi($sisi1, $sisi2, $sisi3){
        $this->sisi1 = $sisi1;
        $this->sisi2 = $sisi2;
        $this->sisi3 = $sisi3;
    }

    public function setAlas($alas){
        $this->alas = $alas;
    }

    public function setTinggi($tinggi){
        $this->tinggi = $tinggi;
    }


    public function getInfo() {
        $str = "Segitiga : " . parent::getLabel() . "| Alas: {$this->alas}" . "| Tinggi: {$this->tinggi}" . "| Sisi1 : {$this->sisi1}" . "| Sisi2 : {$this->sisi2}" . "| Sisi3 : {$this->sisi3} ";
        return $str; 
    }

    public function getKeliling(){
        return $this->sisi1 + $this->sisi2 + $this->sisi3;
    }

    public function getLuas(){
        return ($this->alas*$this->tinggi) / 2;
    }
}
//variabel yang mengarah ke class child
$persegiPanjang = new PersegiPanjang ("nama", "warna", 0, 0);
$lingkaran = new Lingkaran ("nama", "warna", 0, 3.14 );
$segitiga = new Segitiga ("nama", "warna", 0, 0, 0, 0, 0);


//untuk mengatur properti pada bangun datar
$persegiPanjang->setNama("Persegi Panjang");
$persegiPanjang->setWarna("Ungu");
$persegiPanjang->setPanjang(20);
$persegiPanjang->setLebar(10);
echo $persegiPanjang->getInfo();
echo "<br>";
echo "Keliling : " . $persegiPanjang->getKeliling();
echo "| Luas : " . $persegiPanjang->getLuas();
echo "<hr>";

$lingkaran->setNama("Lingkaran");
$lingkaran->setWarna("Cokelat");
$lingkaran->setr(15);
echo $lingkaran->getInfo();
echo "<br>";
echo "Keliling : " . $lingkaran->getKeliling();
echo "| Luas : " . $lingkaran->getLuas();
echo "<hr>";

$segitiga->setNama("Segitiga");
$segitiga->setWarna("Hitam");
$segitiga->setAlas(4);
$segitiga->setTinggi(8);
$segitiga->setSisi(3, 4, 5);
echo $segitiga->getInfo();
echo "<br>";
echo "Keliling : " . $segitiga->getKeliling();
echo "| Luas : " . $segitiga->getLuas();
echo "<hr>";

?>